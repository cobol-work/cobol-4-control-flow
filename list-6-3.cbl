       IDENTIFICATION DIVISION.
       PROGRAM-ID. LIST-6-3.
       AUTHOR. VIRAPAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  REP-COUNT         PIC 9(4). 
       01  PRN-PER-COUNT     PIC Z,ZZ9.
       01  NUMBER-OF-TIMES   PIC 9(4) VALUE 1000.

       PROCEDURE DIVISION.
       BEGINPROGRAM.
           PERFORM VARYING REP-COUNT FROM 0 BY 50
                 UNTIL REP-COUNT  = NUMBER-OF-TIMES 
                 MOVE REP-COUNT  TO PRN-PER-COUNT
                 DISPLAY "Counting : " PRN-PER-COUNT  
           END-PERFORM
           MOVE  REP-COUNT  TO PRN-PER-COUNT
           DISPLAY "If i have told you once , "
           DISPLAY "I've told you " PRN-PER-COUNT  " times."
           GOBACK   
           .   