       IDENTIFICATION DIVISION.
       PROGRAM-ID. SQUARE-STAR.
       AUTHOR. VIRAPAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  SCR-LINE       PIC   X(80).
       01  STAR-NUM       PIC   9(3)  VALUE 0.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-STAR-INPUT THRU 001-EXIT
           PERFORM 001-PRINT-STAR-LINE THRU 001-EXIT STAR-NUM TIMES
           GOBACK
       .
       
       001-PRINT-STAR-LINE.
           MOVE ALL "*" TO SCR-LINE(1:STAR-NUM)
           DISPLAY SCR-LINE
           .

       001-STAR-INPUT.
           PERFORM UNTIL STAR-NUM > 0
              DISPLAY "Enter number of star: " WITH NO ADVANCING
              ACCEPT STAR-NUM
              IF STAR-NUM = 0 
                 DISPLAY "Please input star number in positive number"
           END-PERFORM
       .


       001-EXIT.
           EXIT.
