       IDENTIFICATION DIVISION.
       PROGRAM-ID. LISTING-6-4.
       AUTHOR. VIRAPAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  COUNTERS.
           05 HUNDREDS-COUNT    PIC   99 VALUE ZEROS.
           05 TENS-COUNT        PIC   99 VALUE ZEROS.
           05 UNITS-COUNT       PIC   99 VALUE ZEROS.

       01  ODOMETER.
           05 PRN-HUNDREDS      PIC   9.
           05 FILLER            PIC   X  VALUE "-".
           05 PRN-TENS          PIC   9.
           05 FILLER            PIC   X  VALUE "-".
           05 PRN-UNTILS        PIC   9.

       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Using an out-of-line Perform"
           PERFORM COUNT-MILEAGE
            VARYING HUNDREDS-COUNT FROM 0 BY 1 UNTIL HUNDREDS-COUNT > 9
            AFTER   TENS-COUNT  FROM  0 BY 1 UNTIL TENS-COUNT > 9
            AFTER   UNITS-COUNT FROM  0 BY 1 UNTIL UNITS-COUNT > 9

      *    DISPLAY "Now using in-line Perform"
      *    PERFORM VARYING HUNDREDS-COUNT FROM 0 BY 1 
      *    UNTIL HUNDREDS-COUNT > 9
      *       PERFORM VARYING TENS-COUNT FROM 0 BY 1 
      *       UNTIL TENS-COUNT > 9
      *          PERFORM  VARYING UNITS-COUNT FROM 0 BY 1 
      *          UNTIL UNITS-COUNT > 9
      *             MOVE HUNDREDS-COUNT TO PRN-HUNDREDS
      *             MOVE TENS-COUNT TO PRN-TENS
      *             MOVE UNITS-COUNT TO PRN-UNTILS
      *             DISPLAY "In - " ODOMETER
      *          END-PERFORM
      *       END-PERFORM
      *    END-PERFORM

           DISPLAY "End of odometer simulation."
           GOBACK.

       COUNT-MILEAGE.
           MOVE HUNDREDS-COUNT  TO PRN-HUNDREDS
           MOVE TENS-COUNT      TO PRN-TENS
           MOVE UNITS-COUNT     TO PRN-UNTILS 

           DISPLAY "Out - " ODOMETER. 

