       IDENTIFICATION DIVISION.
       PROGRAM-ID. TRANGLE-1.
       AUTHOR. VIRAPAT.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  SCR-LINE       PIC   X(80).
       01  STAR-NUM       PIC   9(3)  VALUE 0.
           88 VALID-STAR       VALUE 1 THRU 80.
       01  INDEX-NUM      PIC   9(3)  VALUE 0.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-STAR-INPUT THRU 001-EXIT
           PERFORM 001-PRINT-STAR-LINE THRU 001-EXIT 
           VARYING INDEX-NUM FROM 1 BY 1 UNTIL INDEX-NUM > STAR-NUM 
           GOBACK
       .
       
       001-PRINT-STAR-LINE.
           
           MOVE ALL "*" TO SCR-LINE(1:INDEX-NUM)
           DISPLAY SCR-LINE
           .

       001-STAR-INPUT.
           PERFORM UNTIL VALID-STAR
              DISPLAY "Enter number of star: " WITH NO ADVANCING
              ACCEPT STAR-NUM
              IF NOT VALID-STAR 
                 DISPLAY "Please input star number in positive number"
           END-PERFORM
       .


       001-EXIT.
           EXIT.
